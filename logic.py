import numpy as np


def solver(A: np.array, b: np.array) -> np.array:
    """Vyřeší soustavu rovnic"""

    s_matrix_A = np.shape(A)[0]  # Rozměr matice A
    s_matrix_b = np.shape(b)[1]  # Rozměr matice B

    # Dopředná eliminace
    for j in range(0, s_matrix_A):  # sloupce

        # Částečný výběr hlavního prvku
        # index radku s nejvetším prvkem
        idx_max_radek = np.argmax(A[j:s_matrix_A, j])
        idx_max_radek = idx_max_radek + j
        # prohození řádků
        A[[idx_max_radek, j]] = A[[j, idx_max_radek]]
        b[[idx_max_radek, j]] = b[[j, idx_max_radek]]

        for i in range(j+1, s_matrix_A):  # radky
            m = A[i, j] / A[j, j]  # výpočet multiplikátoru

            for k in range(j, s_matrix_A):  # eliminace radku ## dodelat
                A[i, k] = A[i, k] - A[j, k]*m

            for k in range(0, s_matrix_b):
                b[i, k] = b[i, k] - b[j, k]*m

    # Zpětná eliminace
    for j in range(s_matrix_A-1, 0, -1):
        for i in range(0, j):
            m = A[i, j] / A[j, j]
            A[i, j] = A[i, j] - A[j, j]*m

            for k in range(0, s_matrix_b):
                b[i, k] = b[i, k] - b[j, k]*m

    # Vytvoření matice výsledků
    ans = np.zeros(shape=(s_matrix_A, s_matrix_b))

    for j in range(0, s_matrix_b):
        for i in range(0, s_matrix_A):
            ans[i, j] = b[i, j] / A[i, i]

    return ans


def result(matrix_a: str, matrix_b: str, matrix_solution: str, comment: str, success: bool) -> dict:
    """Vytvoří objekt ze vstupních dat"""
    data = dict()
    data["matrix_a"] = matrix_a
    data["matrix_b"] = matrix_b
    data["matrix_solution"] = matrix_solution
    data["comment"] = comment
    data["success"] = success
    return data


def format_matrix(matrix: np.matrix) -> str:
    """Převede numpy matrix do hezkého stringu"""
    return np.array2string(
        matrix, suppress_small=True).replace("[", " ").replace("]", " ")


def main(A_string: str, b_string: str) -> dict:
    """
    Ověří vstupní data,
    ověří jestli má zadaná soustava řešení
    vrátí dict s daty o řešení soustavy """

    if A_string == "" or b_string == "":
        return result(A_string,
                      b_string,
                      "No solution",
                      "The input matrices are empty", False)

    # Načteme rovnice ze strngů do matic
    try:
        A_matrix = np.matrix(A_string, dtype=float)
        b_matrix = np.matrix(b_string, dtype=float)
    except (ValueError, SyntaxError):
        return result(A_string,
                      b_string,
                      "No solution",
                      "The input matrices are invalid", False)

    # Převedeme numpy matice do hezkého stringu
    A_pretty = format_matrix(A_matrix)
    b_pretty = format_matrix(b_matrix)

    # Ověříme jestli je matice A je čtvercová
    if np.shape(A_matrix)[0] != np.shape(A_matrix)[1]:
        return result(A_pretty,
                      b_pretty,
                      "No solution",
                      "The matrix A must be a square matrix",
                      False)

    # Ověříme jestli matice A má stejný rozměr jako je výška matice b
    if np.shape(A_matrix)[0] != np.shape(b_matrix)[0]:
        return result(A_pretty,
                      b_pretty,
                      "No solution",
                      "The matrix sizes are different",
                      False)

    # Ověříme, jestli má soustava řešení
    if np.linalg.det(A_matrix) == 0:
        return result(A_pretty,
                      b_pretty,
                      "No solution",
                      "Given system has no solution. Determinant is zero!",
                      False)

    # Vyřešíme soustavu
    solution = solver(A_matrix, b_matrix)
    solution_pretty = format_matrix(solution)
    return result(A_pretty,
                  b_pretty,
                  solution_pretty,
                  "Calculation was successful",
                  True)
