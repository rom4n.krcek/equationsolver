const params = new Proxy(new URLSearchParams(window.location.search), {
    get: (searchParams, prop) => searchParams.get(prop),
});

matrix_a = document.getElementById("data-matrix-a")
matrix_a.innerText = params.matrix_a
matrix_b = document.getElementById("data-matrix-b")
matrix_b.innerText = params.matrix_b


//    A = np.matrix("10 -7 0; -3 2.099 6; 5 -1.1 4.8", dtype=float)
// b = np.matrix("7; 3.901; 5.9", dtype=float)