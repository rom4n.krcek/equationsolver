# Řešič soustavy lineárních rovnic

Odkaz na řešič [ZDE](https://orebolt.cz/input.html)

Jako semestrální projekt do VP0 jsem vypracoval řešič soustavy lineárních rovnic. 
K řešení používám Gausovu eliminační metodu s částečným výběrem hlavního prvku.

## Gausova eliminační metoda 
Je to jedna z metod řešení soustavy lineárních rovnic. Řešení spočívá v převedení matice 
soustavy A na horní trojúhelníkovou matici a následnou zpětnou substituci a získání hodnot 
proměnných. 

Metoda částečného výběru hlavního prvku spočívá v prohození k-tého řádku s r-tým řádek, 
který má v k-tém sloupci největší absolutní hodnotu 
koeficientu a zároveň r-tý musí ležet pod k-tým řádkem. Tím dosáhneme nejvyšší možnou 
velikost pivotu a nejmenší možnou velikost multiplikátoru 

## Implementace 
Samotný řešič je napsaný v pythonu, ale data do řešiče se zadávají z webového rozhraní.
To pomocí pyscriptu naimportuje řešič jako knihovnu a předá mu data zadané uživatelem.


## Popis funkcí

```python
def main(A_string: str, b_string: str) -> dict:
```
Je to hlavní funkce knihovny. Tuto fuknci volá pyscript. 
Vstupem jsou dva stringy které obsahují matici soustavy a matici pravé strany ve formátu jako když je zadáváme do matlabu. Samotná fuknce ověří, jestli je vstup opravdu matice, zda-li se matice shodují v rozměrech a zda-li má soustava vůbec řešení. Funkce vrací dictionary, ve které jsou naformátované matice a slovní komentář, který shnuje jestli bylo možné řešení dosáhnout. 


```python
def solver(A: np.array, b: np.array) -> np.array:
```
Samotná fuknce, která řeší soustavu. Vstupem jsou dvě matice typu ```np.matrix```. Výsledkem je matice řešení soustavy stejného typu.


## Závěr
Cílem tohoto projektu bylo naprogramovat řešic systému lineárních rovnic v programovacím jazyku python. Konkrétně jsem použil framework **pyscipt** abych k samotnému řešiči mohl ještě vytvořit uživatelské rozhraní.
Pyscipt je nový framework, který umožňuje spouštět python kód v prohlížeči. Vzhledem k tomu, že je to nový projekt tak na webu není tak moc ukázek, takže to není ideální projekt pro začátečníky. Celý projekt jsem testoval řadou různých soustav, u kterých jsem znal řešení a takové celou řadou chybných vstupů. A vždy jsem dostal správné řešení, nebo alespoň informaci o tom kde jsem v zadávání udělal chybu.
